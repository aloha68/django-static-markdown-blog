# Changelog
All notable changes to this project will be documented in this file.

## 0.1.2 - 2019-09-26

### Added

- An option to show or not the breadcrumb (default is visible if the current file has a parent)

## 0.1.1 - 2019-09-26

### Added
- Add [django_leaflet_gpx](https://gitlab.com/aloha68/django-leaflet-gpx) app as side project to handle Leaflet-JS map
- Default value for BLOG_INDEX_VIEW settings (main.md)
- New two settings BLOG_CACHE and BLOG_CACHE_OPTIONS to manage blog cache
- Load highlight-js only if there is some code on the page
- Include all highlight-js CSS style in the application repository

### Changed
- Update highlight-js to version 9.15.8

### Removed
- Remove all Leaflet-JS things from this application (replaced by *django_leaflet_gpx*)

## 0.1.0 - 2019-06-19

### Changed
- Update to django 2.2
- Update to python 3.5

## 0.0.2 - 2019-04-27

### Added
- Syntax highlighting 

### Changed
- Show GPX average speed with decimals if lower than 10km/h

## 0.0.1 - 2018-08-20
First release
